# Variscite Yocto Linux Build for Tempest

## Prerequisites  
We use the Variscite Docker images to have the correct build environment

 :warning: **Do NOT run as ROOT, Bitbake will not work.**  :warning:
 
### install docker tools
```
$ sudo apt update && sudo apt install docker.io qemu-user-static
$ sudo usermod -aG docker ${USER}
$ sudo apt install linux-headers-$(uname -r)
```
Logout and login again for permissions to take effect.

### Clone Docker repo
```
git clone https://github.com/JeroenVandezande/var-host-docker-containers.git ~/var-host-docker-containers
```
### Prepare Shared Folder

```
$ cd ~
$ mkdir docker-yocto-build
```

## Build Process

### Start Docker

```
$ cd var-host-docker-containers/
$ ./run.sh -p -u 20.04 -v ~/docker-yocto-build:/home/vari -v /opt:/opt -v /dev:/dev
```
After this command you should be in the Docker instance

:bulb: When running *sudo* command the requested password is *ubuntu*

## Getting the Meta-Data
This step uses the Google Repo tool to clone a list of repositories defined using an XML file. In our case the tempest.xml file in this repository.
These repositories contain Yocto recipes that will be used later on in the build process.


### Download the Meta Data:

```
$ mkdir ~/var-fslc-yocto
$ cd ~/var-fslc-yocto
$ repo init -u https://bitbucket.org/goldstandarddiagnostics/yoctolinuxvariscitemodules.git -b fsl-gatesgarth -m tempest.xml
$ repo sync -j4
```

At the end of the commands you have every metadata you need to start work with.

## Build Process

### To start a simple image build:

```
$ MACHINE=imx8mp-var-dart DISTRO=fsl-imx-xwayland . var-setup-release.sh -b build_xwayland
$ bitbake fsl-image-gui
$ bitbake var-image-swupdate
```

### To Start a SWUpdate image build:

```
$ MACHINE=imx8mp-var-dart DISTRO=fsl-imx-xwayland . var-setup-release.sh -b build_xwayland
$ bitbake var-image-swu
```

The last line will give you the resulting SWUpdate files.
You can use these files by going to the SWUpdate webinterface (http running on port 8080)
Or the files can be used in an integrated upgrade system

### To re-build:

First we need to re-sync the repos:
```
$ cd ~/var-fslc-yocto
$ repo init -u https://bitbucket.org/goldstandarddiagnostics/yoctolinuxvariscitemodules.git -b fsl-gatesgarth -m tempest.xml
$ repo sync -j4
```

Then we clean the old config data:
```
$ cd build_xwayland/
$ rm conf -r
$ cd ..
```

Then we re-start the build:
```
$ MACHINE=imx8mp-var-dart DISTRO=fsl-imx-xwayland . var-setup-release.sh -b build_xwayland
$ bitbake fsl-image-gui
$ bitbake var-image-swupdate
```

## Getting the Disk Image

The Diskimage can be created after a succesfull build 

The first time you need to make the included script executable:
```
$ cd ~/var-fslc-yocto
$ sudo chmod +x creatediskimage.sh 
```

Then you can run the script:
```
$ cd ~/var-fslc-yocto
$ sudo ./creatediskimage.sh 
```

The resulting .xz file can be downloaded from the build computer and burned to an SD card using Balana Etcher or similar program.

## Burning SD card to internal MMC of SOM Module
Boot the SD card on the SOM module (make sure the SOM is configured to boot from SD)
after booting issue this command:
```
$ install_yocto.sh -u
```

:bulb: The "-u" option make the script create the double-copy compatible layout needed for SWUpdate

**If this gives an error, try running the command again, it will work the second time**


After this command finishes turn off the SOM and eject the SD card. Switch the SOM to internal Boot mode and power up again.
